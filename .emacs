;;Add package archive links
(require 'package)
(package-initialize)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
			 ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))


;;Paths
(add-to-list 'custom-theme-load-path "/Users/mateorojas/Documents/shell/Emacs/blackboard-theme")
;;Themes

(load-theme 'blackboard t)
;;(load-theme 'subatomic t)
;; (load-theme 'noctilux t) cool dark
;; (load-theme 'solarized-dark t)
;; (load-theme 'zenburn t)
;; (load-theme 'purple-haze t)

;;Comment region 
(global-set-key [?\C-x?\C-\;] 'comment-or-uncomment-region)
;;(put 'upcase-region 'disabled nil)
;; alt to cmd on mac keyboard
(setq mac-option-modifier nil
      mac-command-modifier 'meta
      x-select-enable-clipboard t)

;;Show line numbers
(global-linum-mode t)

